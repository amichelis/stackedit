
# Neurons

## Sensory

* **DIT** : Top Boundry Distance
* **DIB** : Bottom Boundry Distance
* **DIL** : Left Boundry Distance
* **DIR** : Right Boundry Distance
* **PRD** : Distance of the closest organism
* **PRO** : Distance of the closest other-gender organism
* **PRS** : Distance of the closest same-gender organism
* **PRF** : Distance of the closest food item
* **CK1** : Oscilator T = 2 steps
* **CK2** : Oscilator T = 3 steps
* **CK3** : Oscilator T = 5 steps
* **CK4** : Oscilator T = 7 steps
* **CK5** : Oscilator T = 11 steps
* **CK6** : Oscilator T = 13 steps
* **CK7** : Oscilator T = 17 steps
* **CK8** : Oscilator T = 19 steps

## Internal

* **IR[0-7]** : Internal Ramp 0-7
* **IB[0-7]** : Internal Binary 0-7
* **IN[0-7]** : Internal Negative 0-7

## Actuators

* **MVT** : Move Top
* **MVB** : Move Bottom
* **MVL** : Move Left
* **MVR** : Move Right
* **MTE** : Mate 
* **EAT** : Pick food
* **ASK** : Ask for 1 Food 
* **KIL** : Kill 

> Written by Andreas Michelis.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTE0OTkxNzIzLC0xNDExMTI2MzMyLDE0MT
U0NjQyNjcsLTIzNDg4MTExNCwxOTU0MzY4NTEwLC0yMDU2MzY5
MzQ1LDEyMTg4MjQ1MDldfQ==
-->