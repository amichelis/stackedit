
# Web Page in a nutshell

This document contains basic information about creating a webpage, including the usage of `PHP` and interactions with `PosgreSQL` or `MySQL` databases.

<br/><br/>

## Prerequisites

Basic information that you need to have access to are:

* **Database**
  
  | INFO TITLE | INFORMATION DESCRIPTION OR VALUE |
  |-:|:-|
  | Database Host | usually `localhost` |
  | Database Port | MySQL: `3306` - PosgreSQL: `5432` |
  | Database Username | _the database username that has been provided to you by your institute_ |
  | Database Password | _the database password that has been provided to you by your institute_ |
  | Database Name | _the database name that has been provided to you by your institute_ |

* **Server**
  
  | INFO TITLE | INFORMATION DESCRIPTION OR VALUE |
  |-:|:-|
  | VPN Credentials | _Follow course's instructions about how to access the institute's server_     |
  | Domain Name | _The name of your site (ie._ `http://www.myInstitute.edu/courseId/groupX/`_)_ |
  | Web Root (wwwroot) | _The directory where you should place all your site files_ |

<br/><br/><br/><br/>

## Different Parts of a Web-Page

A Web-Page -and, especially a dynamic one, with user base and responsiveness- consists of 3 main things:

1. Front-End
2. Data Storage
3. Back-End

<br/><br/>

### Front-End

The _Front-End_ of a web page is what an end-user sees. What shapes, letters, buttons fields etc. are being displayed to the user's screen, when he goes on any of the pages that exist on `http://myFancyWebsite.com`. This usually is written-in/translated-to `HTML`, `CSS` that describes how all these should look like, and little help of `Javascript` (or _Javascript frameworks_ such as `JQuery`) to add extra responsiveness/ease-of-use to the page.

As "the legend goes for centuries". _HTML is not a programming language_. It is merely a _markup language_ that provides a notation for describing design Elements, as well as some hooks for the javascript-part of the equation. In general, standalone entites have the notation

```html
<Element options/>
```
and nesting Elements have the notation 

```html
<Element options>
	[child-content]
</Element>
```
Some examples of Elements that can be described within html are the following

| Element Notation | Element Description |
|:-:|:-|
|`<div [options]></div>` | A **div**ision/section of the page that can have specific shape, location and design. This Element can host any type of content: Headings, Paragraphs, buttons, etc. |
|`<p [options]></p>`| **P**aragraph. This Element is used to describe and separate visually paragraphs |
|`<h# [options]></h#>`| **H**eadings `h1` through `h5` different sizes of headings/sub-headings that enables developer to create titles. |
|`<span [options]></span>`| `span` is an in-line Element that provides to the text different design capabilities than the rest of the text. |
|`<button [options]></button>`| An Element that's being translated into a button |
|`<input [options]/>`| An Element that's being translated input box |

A very important note is that, unless inside a `<pre></pre>` Element, new lines are not being parsed by the browser. That means that, whenever you need to force a new line, you must to use the standalone Element `<br/>` which stands for _(line) **br**eak_ .

More info about the different Elements that exist within `html` can be found at [W3SCHOOLS](https://www.w3schools.com/html/html_elements.asp) or [Sheldon Brown](https://sheldonbrown.com/web_sample1.html).

<br/><br/><br/>

### Data Storage

The mechanism which takes care of secure data storage, access and modification is called _Database_. Databases are using a user-based model to authorize/block data access and it's usually detatched from the rest of the web site (a.k.a. It runs as a seperate _service_  that our Apps can _communicate_ with.

There are a lot of different models and technologies of Databases, but the most known and easy to cope with are the **Relational** databases.

Relational databases struct data in table-like strutures where, each row contains related information, divided in cells.

If we wanted to store different car informations in python, we would define a `Car` class with the fields we want to store about each car:

```python
class Car:
	def __init__(self, plateNumber, brand, horsePower, color):
		self.PlateNumber = plateNumber
		self.Brand = brand
		self.HorsePower = HorsePower
		self.Color = color
``` 

If we wanted to see if two variables are about the same car, all we needed to check is if their Plate Numbers are equal. So, we would extend it like this:

```python
class Car:
	def __init__(self, plateNumber, brand, horsePower, color):
		self.PlateNumber = plateNumber
		self.Brand = brand
		self.HorsePower = HorsePower
		self.Color = color

	def Equals(self, otherCar)
		return otherCar.plateNumber == self.PlateNumber
``` 

The exact same fashion is followed on relational databases. We have a table named Cars:

|PlateNumber|Brand|HorsePower|Color|
|-|-|-|-|
|ABC-1234|Subaru|120|Blue|
|DEF-5678|Toyota|100|Gray|
|GHI-1000|Audi|165|Black|

Databases should be as space-eficient as they can be and as deterministic as possible. To achieve that, we define **Primary Keys**: Columns that are unique to each row and cannot be the same for two different rows. On the example above, The primary key could be our Plate Number

|**PlateNumber**|Brand|HorsePower|Color|
|-|-|-|-|
|**ABC-1234**|Subaru|120|Blue|
|**DEF-5678**|Toyota|100|Gray|
|**GHI-1000**|Audi|165|Black|

If we ever try to add the same car twice, we'll get an error explaining that a row with the same key already exists, and if we ever try to get a car from the table given its plate number, we would know:

1) If no value is returned, the car is surely not registered
2) If the car is registered, we would get exactly one car which
	will surely be the one we're asking for.
3) If we wanted to change the color of the car, we would just
	need to provide it's plate number and the new color

	---

In order to communicate with our database, we have to use **SQL (Structured Query Language)** - usually red as "_sequel_" - which describes the action we want to perform on the database in a way that closely resembles "english".

To create a table in this language, we would type:

```sql
CREATE TABLE `Cars` (
	`PlateNumber`   VARCHAR(8)      NOT NULL,
	`Brand`         VARCHAR(16)     NOT NULL,
	`HorsePower`    INT             NOT NULL,
	`Color`         VARCHAR(16)     NOT NULL,

	PRIMARY KEY (`PlateNumber`)
);

--	Which stands for:
--		Create a table named cars, which contains:
--
--			a text column of maximum lenght of 8,
--			named "PlateNumber" that must be not empty
--
--			a text column of maximum lenght of 16,
--			named "Brand" that must be not empty
--
--			a integer column named "HorsePower" that
--			must be not empty
--
--			a text column of maximum lenght of 16,
--			named "Color" that must be not empty
--
--		And this table has "PlateNumber" column as its
--		primary key
```



To Add a new Car to the database, we could type:

```sql
INSERT INTO `Cars`
    (`PlateNumber`,`Brand`,`HorsePower`,`Color`) 
VALUES 
	('ABC-1234', 'Subaru', 120, 'Blue'),
	('DEF-5678', 'Toyota', 100, 'Gray') 
    -- [...]
;

--	Which stands for:
--		Add the following rows inside "Cars" table:
--
--			Row 1:
--				PlateNumber : ABC-1234
--				Brand 		: Subaru
--				HorsePower	: 120
--				Color		: Blue
--
--			Row 2:
--				PlateNumber : DEF-5678
--				Brand 		: Toyota
--				HorsePower	: 100
--				Color		: Gray
---------------------------------------------------
-- It must be one or more rows, even though the example
-- adds two rows.
```

To select and fetch row(s) from the table:

```sql
-- Bring every row from "Cars" Table
SELECT * 
FROM`Cars`;
-------------------------------------------------
-- Bring a list of all PlateNumbers and their 
-- corresponding HorsePower from "Cars" Table
SELECT `PlateNumber`, `HorsePower` 
FROM `Cars`;
-------------------------------------------------
-- Bring a list of all PlateNumbers of cars 
-- with blue color
SELECT `PlateNumber` 
FROM `Cars` 
WHERE `Color` = 'blue';
-------------------------------------------------
-- Bring a list of all Brands and the
-- average HorsePower each brand offers
-- stored inside a column named "AvgHorsePower"
SELECT 		`Brand`, avg(HorsePower) as `AvgHorsePower`
FROM 		`Cars` 
GROUP BY 	`Brand`;
-------------------------------------------------
-- Bring a list of all existing colors from "Cars" Table
SELECT DISTINCT `Color` 
FROM `Cars`;
-------------------------------------------------
-- Bring a list of all PlateNumbers of cars 
-- whose PlateNumber starts with "A"
SELECT `PlateNumber` 
FROM `Cars` 
WHERE `PlateNumber` LIKE 'A%';
```

<br/><br/><br/>

### Back-End

> To be written
<!--stackedit_data:
eyJoaXN0b3J5IjpbNDg2MTc1MjQxLC0yNDExOTA2MDgsLTI0MT
E5MDYwOCw3MjE1MzUxMTAsMTg0MzM3Mjg5NCwtODAzNTI2MjY1
LDEwODE5NDAwNzQsLTE1MjkxNTQ3MCwxMjM5NjYyODYxLDEwMT
A3NjcwMjgsLTEzMzA2NjcxMzgsMTM2NTc3NDcwNSwtMTk1NTgx
OTYyNCwxMjI3MDIzODkzLDEyMzI5MzM0NzNdfQ==
-->